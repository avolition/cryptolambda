const data = require('./test.data.js');
const { performance } = require('perf_hooks');
const numeral = require('numeral');

// console.log(performance);

let total = 0;

const iterations = 100;

for (let i = 0; i < iterations; i++) {
  performance.mark(`loop ${i} start`);

  let formattedData = [];

  // formattedData = data.reduce((acc, entry) => {
  //   const newObject = {};
  //   Object.keys(entry).forEach(key => {
  //     let upKey = key.toUpperCase();
  //     if (upKey === 'NAME') {
  //       return;
  //     } else if (numeral(entry[key])._value != null) {
  //       newObject[upKey] = numeral(entry[key])._value;
  //     } else {
  //       newObject[upKey] = entry[key];
  //     }
  //   });
  //   acc.push(newObject);
  //   return acc;
  // }, []);
  // 1x iterations average time === 151.74252200126648
  // 10x iterations average time === 846.7047886469147
  // 100x iterations average time === 6883.278883181586
  // 1000x iterations average time === 71186.52959713229

  // formattedData = data.map(entry => {
  //   let newObject = {};
  //   Object.keys(entry).forEach(key => {
  //     let upKey = key.toUpperCase();
  //     if (upKey === 'NAME') {
  //       return;
  //     } else if (numeral(entry[key])._value != null) {
  //       newObject[upKey] = numeral(entry[key])._value;
  //     } else {
  //       newObject[upKey] = entry[key];
  //     }
  //   });
  //   return newObject;
  // });
  // 1x iterations average time === 157.79106450080872
  // 10x iterations average time === 873.7172015146775
  // 100x iterations average time === 7252.581488992908
  // 1000x iterations average time === 70216.19618527092

  formattedData = data.map(entry => {
    let newObject = {};
    Object.keys(entry).forEach(key => {
      let upKey = key.toUpperCase();
      if (entry[key] === null) {
        newObject[upKey] = null;
      } else if (+entry[key] === NaN) {
        newObject[upKey] = entry[key];
      } else {
        newObject[upKey] = +entry[key];
      }
    });
    delete newObject['NAME'];
    return newObject;
  });
  // if (formattedData.length !== 2079) console.log(formattedData.length);

  performance.mark(`loop ${i} end`);
  performance.measure('Inputs validation', `loop ${i} start`, `loop ${i} end`);
  total += performance.nodeTiming.duration || 0;
}
console.log(total / (iterations + 1));

// let total2 = 0;

// for (let i = 0; i < iterations; i++) {
//   performance.mark(`loop ${i} start`);

//   const STATE = data.reduce((agg, pair) => {
//     STATE[pair] = { s: pair, hist: [] };
//   });

//   performance.mark(`loop ${i} end`);
//   performance.measure('Inputs validation', `loop ${i} start`, `loop ${i} end`);
//   const measurements = performance.getEntriesByType('measure');
//   total2 += measurements[0].duration || 0;
// }
// console.log(total2 / (iterations + 1));
