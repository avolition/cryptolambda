(async function() {
  /* NOT FOR PROD */ require('dotenv').config({ path: 'variables.env' });

  // store ALL env vars for use throughout script
  // permanent
  const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE;
  const POSTGRES_USER = process.env.POSTGRES_USER;
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
  const POSTGRES_HOST = process.env.POSTGRES_HOST;
  // function specific
  const DB_TABLE_NAME = process.env.DB_TABLE_NAME;
  const N = process.env.DECIMAL_MAX_DIGITS;

  // import packages
  const axios = require('axios');
  const pg = require('pg');
  const Sequelize = require('sequelize');
  const numeral = require('numeral');

  // initialize sequelize w db config
  const sequelize = new Sequelize(
    POSTGRES_DATABASE,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    {
      host: POSTGRES_HOST,
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      },
      operatorsAliases: false,
      logging: false
    }
  );

  // define table schema for this api
  const table_schema = sequelize.define(DB_TABLE_NAME, {
    UUID: {
      type: Sequelize.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    ID: Sequelize.STRING,
    SYMBOL: Sequelize.STRING,
    RANK: Sequelize.SMALLINT,
    PRICE_USD: Sequelize.DECIMAL(N, 8),
    PRICE_BTC: Sequelize.DECIMAL(N, 8),
    '24H_VOLUME_USD': Sequelize.BIGINT,
    MARKET_CAP_USD: Sequelize.BIGINT,
    AVAILABLE_SUPPLY: Sequelize.BIGINT,
    TOTAL_SUPPLY: Sequelize.BIGINT,
    MAX_SUPPLY: Sequelize.BIGINT,
    PERCENT_CHANGE_1H: Sequelize.DECIMAL(10, 2),
    PERCENT_CHANGE_24H: Sequelize.DECIMAL(10, 2),
    PERCENT_CHANGE_7D: Sequelize.DECIMAL(10, 2),
    LAST_UPDATED: Sequelize.INTEGER
  });

  // api response var
  let apiRes;
  // var for storing formatted api data;
  let formattedData = [];
  // error handling vars
  let errArr = [],
    thisErr = [];

  // pull api data and save for DB update
  try {
    apiRes = await axios.get(
      'https://api.coinmarketcap.com/v1/ticker/?limit=0'
    );
    // console.log(`1: 1st api response entry: ${apiRes.data[0]}`);
    formattedData = apiRes.data.map(entry => {
      let newObject = {};
      Object.keys(entry).forEach(key => {
        let upKey = key.toUpperCase();
        if (upKey === 'NAME') {
          return;
        } else if (numeral(entry[key])._value != null) {
          newObject[upKey] = numeral(entry[key])._value;
        } else {
          newObject[upKey] = entry[key];
        }
      });
      return newObject;
    });
    // console.log(`2: Formatted api data: ${formattedData[0]}`);
  } catch (err) {
    thisErr = [`ERROR: accessing api: `, err];
    errArr.push(thisErr);
    // console.log(thisErr);
  }

  // eval(require('locus'));

  // sequelize async / await enabler var
  let tx;

  // save data to DB
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await table_schema.sync({ force: false }, tx);
    await table_schema.bulkCreate(formattedData, tx).then((data, tx) => {
      console.log(`3: New data successfully saved to DB: ${DB_TABLE_NAME}`);
    });
    await tx.commit();
  } catch (err) {
    thisErr = [`ERROR: saving data to DB ${DB_TABLE_NAME}: `, err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // close off all connections to db
  await sequelize.close();
  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    throw errArr;
  } else {
    return 'function completed';
  }
})();
