(async function() {
  /* NOT FOR PROD */ require('dotenv').config({ path: 'variables.env' });

  // store ALL env vars for use throughout script
  // permanent
  const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE;
  const POSTGRES_USER = process.env.POSTGRES_USER;
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
  const POSTGRES_HOST = process.env.POSTGRES_HOST;
  // function specific
  const DB_TABLE_NAME = process.env.DB_TABLE_NAME;
  const N = process.env.DECIMAL_MAX_DIGITS;

  // import packages
  const axios = require('axios');
  const pg = require('pg');
  const Sequelize = require('sequelize');
  const numeral = require('numeral');

  // initialize sequelize w db config
  const sequelize = new Sequelize(POSTGRES_DATABASE, POSTGRES_USER, POSTGRES_PASSWORD, {
    host: POSTGRES_HOST,
    dialect: 'postgres',
    pool: {
      max: 5,
      min: 0,
      acquire: 10000,
      idle: 1000
    },
    operatorsAliases: false
  });

  // define data schema for binance klines table
  const table_schema = sequelize.define(DB_TABLE_NAME, {
    UUID: {
      type: Sequelize.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    SYMBOL: Sequelize.STRING,
    ASK: Sequelize.DECIMAL(N, 8),
    BID: Sequelize.DECIMAL(N, 8),
    LAST_TRADE_CLOSED_PRICE: Sequelize.DECIMAL(N, 8),
    VOL_TODAY: Sequelize.DECIMAL(N, 8),
    VOL_24HR: Sequelize.DECIMAL(N, 8),
    VOL_TODAY_WEIGHTED_AVG_PRICE: Sequelize.DECIMAL(N, 8),
    VOL_24HR_WEIGHTED_AVG_PRICE: Sequelize.DECIMAL(N, 8),
    NUM_TRADES_TODAY: Sequelize.INTEGER,
    NUM_TRADES_24HR: Sequelize.INTEGER,
    LOW_TODAY: Sequelize.DECIMAL(N, 8),
    LOW_24HR: Sequelize.DECIMAL(N, 8),
    HIGH_TODAY: Sequelize.DECIMAL(N, 8),
    HIGH_24HR: Sequelize.DECIMAL(N, 8),
    OPEN_TODAY: Sequelize.DECIMAL(N, 8),
    TIMESTAMP: Sequelize.INTEGER
  });

  // api response var
  let apiRes;
  // allPairs array var
  let allPairs = [];
  // error managing vars
  let errArr = [],
    thisErr = [];

  // ping kraken api to capture list of all pairs
  try {
    // pull complete list of symbols from api
    apiRes = await axios.get('https://api.kraken.com/0/public/AssetPairs');
    console.log('1: allPrices res object length: ', Object.keys(apiRes.data.result).length);

    // save every valid pair to allPairs array
    Object.entries(apiRes.data.result).forEach(entry => {
      !entry[0].includes('.d') ? allPairs.push(entry[0]) : null;
    });
  } catch (err) {
    thisErr = ['ERROR: collecting data from API / creating allPairs array: ', err];
    errArr.push(thisErr);
    // console.log(thisErr);
  }

  // setup object to save data to db
  let formattedData = [];

  // pull api data for all pairs
  try {
    // create api for all pairs and ping
    let api_url = `https://api.kraken.com/0/public/Ticker?pair=${allPairs.join(',')}`;
    // save timestamp for db entries
    let timestamp = Math.round(Date.now() / 1000);
    console.log(`2: GETting data from api_url: ${api_url}`);
    apiRes = await axios.get(api_url);
    // format data to db update
    formattedData = Object.keys(apiRes.data.result).map(pair => {
      let thisPair = apiRes.data.result[pair];
      return {
        SYMBOL: pair,
        ASK: thisPair.a[0],
        BID: thisPair.b[0],
        LAST_TRADE_CLOSED_PRICE: thisPair.c[0],
        VOL_TODAY: thisPair.v[0],
        VOL_24HR: thisPair.v[1],
        VOL_TODAY_WEIGHTED_AVG_PRICE: thisPair.p[0],
        VOL_24HR_WEIGHTED_AVG_PRICE: thisPair.p[1],
        NUM_TRADES_TODAY: thisPair.t[0],
        NUM_TRADES_24HR: thisPair.t[1],
        LOW_TODAY: thisPair.l[0],
        LOW_24HR: thisPair.l[1],
        HIGH_TODAY: thisPair.h[0],
        HIGH_24HR: thisPair.h[1],
        OPEN_TODAY: thisPair.o,
        TIMESTAMP: timestamp
      };
    });
  } catch (err) {
    thisErr = [`ERROR: with API ${api_url} / creating formattedData object: `, err];
    errArr.push(thisErr);
    // console.log(thisErr);
  }

  // sequelize async / await transaction pause var
  let tx;

  // save formattedData to db
  try {
    // initialize sql transaction for saving to db with async / await
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await table_schema.sync({ force: false }, tx);
    await table_schema.bulkCreate(formattedData, tx).then((data, tx) => {
      console.log(`3: New ticker data successfully saved to db: ${DB_TABLE_NAME}`);
    });
    await tx.commit();
  } catch (err) {
    thisErr = [`ERROR: saving new ticker data to db ${DB_TABLE_NAME}: `, err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // close off all connections to db
  await sequelize.close();
  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    throw errArr;
  } else {
    return 'function completed';
  }
})();
