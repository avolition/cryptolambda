(async function() {
  /* NOT FOR PROD */ require('dotenv').config({ path: 'variables.env' });

  // store ALL env vars for use throughout script
  // permanent
  const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE;
  const POSTGRES_USER = process.env.POSTGRES_USER;
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
  const POSTGRES_HOST = process.env.POSTGRES_HOST;
  // function specific
  const DB_TABLE_NAME = process.env.DB_TABLE_NAME;
  const N = process.env.DECIMAL_MAX_DIGITS;
  const STATE_VARS_TABLE_NAME = process.env.STATE_VARS_TABLE_NAME;
  const STATE_VAR_NAME = process.env.STATE_VAR_NAME;
  const TELEGRAM_PAIRS = process.env.TELEGRAM_NOTIFICATION_PAIRS.split(',');
  const HIST_INDEX_1 = parseInt(process.env.HIST_INDEX_1);
  const HIST_PERCENT_1 = parseFloat(process.env.HIST_PERCENT_1).toFixed(2);
  const HIST_INDEX_2 = parseInt(process.env.HIST_INDEX_2);
  const HIST_PERCENT_2 = parseFloat(process.env.HIST_PERCENT_2).toFixed(2);
  const HIST_INDEX_3 = parseInt(process.env.HIST_INDEX_3);
  const HIST_PERCENT_3 = parseFloat(process.env.HIST_PERCENT_3).toFixed(2);
  const HIST_MAX_LENGTH = process.env.HIST_MAX_LENGTH;
  const TELEGRAM_BOT_TOKEN = process.env.TELEGRAM_BOT_TOKEN;
  const TELEGRAM_CHAT_ID = process.env.TELEGRAM_CHAT_ID; // -310814116 for production
  const TELEGRAM_ENABLED = process.env.TELEGRAM_ENABLED;

  // import packages
  const axios = require('axios');
  const pg = require('pg');
  const Sequelize = require('sequelize');

  // initialize sequelize w db config
  const sequelize = new Sequelize(
    POSTGRES_DATABASE,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    {
      host: POSTGRES_HOST,
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        acquire: 10000,
        idle: 1000
      },
      operatorsAliases: false,
      logging: false
    }
  );

  // define state table schema
  const state_table = sequelize.define(STATE_VARS_TABLE_NAME, {
    UUID: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    NAME: Sequelize.STRING,
    DATA: Sequelize.JSON
  });

  // define table schema for this api
  const table_schema = sequelize.define(DB_TABLE_NAME, {
    UUID: {
      type: Sequelize.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    MARKET_NAME: Sequelize.STRING,
    HIGH: Sequelize.DECIMAL(N, 8),
    LOW: Sequelize.DECIMAL(N, 8),
    VOLUME: Sequelize.DECIMAL(N, 8),
    LAST: Sequelize.DECIMAL(N, 8),
    BASE_VOL: Sequelize.DECIMAL(N, 8),
    TIMESTAMP: Sequelize.BIGINT,
    BID: Sequelize.DECIMAL(N, 8),
    ASK: Sequelize.DECIMAL(N, 8),
    OPEN_BUY_ORDERS: Sequelize.INTEGER,
    OPEN_SELL_ORDERS: Sequelize.INTEGER,
    PREV_DAY: Sequelize.DECIMAL(N, 8),
    CREATED: Sequelize.BIGINT
  });

  // fn state var
  let STATE;
  // sequelize async / await transaction pause var
  let tx;
  // error handling vars
  let errArr = [],
    thisErr = [];

  // connect to db > state table and retrieve state object for this loop
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await state_table.sync({ force: false }, tx);
    await state_table
      .findOrCreate({ where: { NAME: `${STATE_VAR_NAME}` }, tx })
      .spread((instance, created, tx) => {
        if (created) {
          console.log('1: state var created');
          STATE = undefined;
        } else {
          let stateVar = instance.get({ plain: true }, tx);
          console.log('1: state var retrieved from db / NOT created');
          STATE = stateVar.DATA;
        }
      });
    // console.log(`2: state object ${STATE_VAR_NAME}: ${STATE}`);
    await tx.commit();
  } catch (err) {
    thisErr = [
      'ERROR: collecting data from state table / parsing state object: ',
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // eval(require('locus'));

  // if STATE doesn't exist yet, populate pairs for monitoring from env var
  if (!STATE) {
    STATE = {};
    TELEGRAM_PAIRS.forEach(pair => {
      STATE[pair] = { s: pair, hist: [] };
    });
    console.log(`3: Created STATE object: `, STATE);
  }

  // if new pair(s) to monitor added to env var, add to STATE for monitoring
  if (TELEGRAM_PAIRS.length > Object.keys(STATE).length) {
    for (let pair of TELEGRAM_PAIRS) {
      if (!STATE[pair]) {
        STATE[pair] = { s: pair, hist: [] };
        console.log(
          `Added new pair ${pair} to STATE as added to TELEGRAM_NOTIFICATION_PAIRS for monitoring`
        );
      }
    }
  }

  // if pair(s) removed from monitor env var, remove from STATE
  if (TELEGRAM_PAIRS.length < Object.keys(STATE).length) {
    for (let pair of Object.keys(STATE)) {
      if (!TELEGRAM_PAIRS.includes(pair)) {
        delete STATE[pair];
        console.log(
          `Deleted pair ${pair} from STATE as removed from TELEGRAM_NOTIFICATION_PAIRS}`
        );
      }
    }
  }

  // api response var
  let apiRes;
  // var for storing formatted api data;
  let formattedData = [];
  // telegram url for sending notifications
  const TELEGRAM_CHAT_URL = `https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage?chat_id=${TELEGRAM_CHAT_ID}&text=`;

  // pull api data and format for saving to db
  try {
    apiRes = await axios.get(
      `https://bittrex.com/api/v1.1/public/getmarketsummaries`
    );
    console.log(`4: res object length: ${apiRes.data.result.length}`);
    for (let entry of apiRes.data.result) {
      // create new state obj for this entry
      let e = {
        MARKET_NAME: entry.MarketName,
        HIGH: entry.High,
        LOW: entry.Low,
        VOLUME: entry.Volume,
        LAST: entry.Last,
        BASE_VOL: entry.BaseVolume,
        TIMESTAMP: new Date(entry.TimeStamp).getTime(),
        BID: entry.Bid,
        ASK: entry.Ask,
        OPEN_BUY_ORDERS: entry.OpenBuyOrders,
        OPEN_SELL_ORDERS: entry.OpenSellOrders,
        PREV_DAY: entry.PrevDay,
        CREATED: new Date(entry.Created).getTime()
      };
      // check if monitoring pair & if change in last x mins should trigger telegram notification
      // if monitoring symbol / pair, populate data for testing
      if (TELEGRAM_ENABLED && STATE[e.MARKET_NAME]) {
        let lp = e.LAST_PRICE;
        if (lp != Infinity && lp != NaN && typeof lp === 'number' && lp > 0) {
          console.log(
            `${e.MARKET_NAME} exists in STATE - adding LAST_PRICE: ${e.LAST}`
          );
          STATE[e.MARKET_NAME].hist.unshift(e.LAST);
          let hist = STATE[e.MARKET_NAME].hist;
          console.log(`LAST_PRICE added: ${hist[0]}`);
          console.log(`${STATE[e.MARKET_NAME]} hist array: ${hist}`);
          // if long enough for comparison, test if index[0] against HIST_INDEX_1 >= HIST_PERCENT_1
          if (hist.length > HIST_INDEX_1) {
            let result1 = (100 / hist[HIST_INDEX_1]) * hist[0] - 100;
            console.log(`result1: ${result1.toFixed(2)}`);
            if (Math.abs(result1) >= HIST_PERCENT_1) {
              console.log(
                `TELEGRAM: ${e.MARKET_NAME} changed ${result1.toFixed(
                  2
                )}% in the last ${HIST_INDEX_1} minute`
              );
              let res = await axios.get(
                `${TELEGRAM_CHAT_URL}+${
                  e.MARKET_NAME
                }+changed+${result1.toFixed(
                  2
                )}%+in+${HIST_INDEX_1}+minute+on+Bittrex`
              );
              console.log(`response1 from TELEGRAM: ${res}`);
            }
            // if length = HIST_INDEX_2, test if index[0] against HIST_INDEX_2 >= HIST_PERCENT_2
            if (hist.length > HIST_INDEX_2) {
              let result2 = (100 / hist[HIST_INDEX_2]) * hist[0] - 100;
              console.log(
                `inputs: HIST_INDEX_2 - STATE[e.MARKET_NAME].hist[HIST_INDEX_2]`
              );
              console.log(`result2: ${result2.toFixed(2)}`);
              if (Math.abs(result2) >= HIST_PERCENT_2) {
                console.log(
                  `TELEGRAM: ${e.MARKET_NAME} changed ${result2.toFixed(
                    2
                  )}% in the last ${HIST_INDEX_2} minutes`
                );
                let res = await axios.get(
                  `${TELEGRAM_CHAT_URL}+${
                    e.MARKET_NAME
                  }+changed+${result2.toFixed(
                    2
                  )}%+in+${HIST_INDEX_2}+minutes+on+Bittrex`
                );
                console.log(`response2 from TELEGRAM: ${res}`);
              }
            }
            // if long enough for comparison, test if index[0] against HIST_INDEX_3 >= HIST_PERCENT_3
            if (hist.length > HIST_INDEX_3) {
              let result3 = (100 / hist[HIST_INDEX_3]) * hist[0] - 100;
              console.log(`result3: ${result3.toFixed(2)}`);
              if (Math.abs(result3) >= HIST_PERCENT_3) {
                console.log(
                  `TELEGRAM: ${e.MARKET_NAME} changed ${result3.toFixed(
                    2
                  )}% in the last ${HIST_INDEX_3} minutes`
                );
                let res = await axios.get(
                  `${TELEGRAM_CHAT_URL}+${
                    e.MARKET_NAME
                  }+changed+${result3.toFixed(
                    2
                  )}%+in+${HIST_INDEX_3}+minutes+on+Bittrex`
                );
                console.log(`response3 from TELEGRAM: ${res}`);
              }
            }
            // if greater than max length, remove last array item
            if (hist.length > HIST_MAX_LENGTH) STATE[e.MARKET_NAME].hist.pop();
          }
        }
      }
      // add to formatted data array
      formattedData.push(e);
    }
  } catch (err) {
    thisErr = [
      `ERROR: retrieving data from api / creating formattedData object: `,
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
  }

  // save new data to db
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await table_schema.sync({ force: false }, tx);
    await table_schema.bulkCreate(formattedData, tx).then((data, tx) => {
      console.log(
        `New /getmarketsummaries data successfully saved to db ${DB_TABLE_NAME}`
      );
    });
    await tx.commit();
  } catch (err) {
    thisErr = [`ERROR: saving new data to db ${DB_TABLE_NAME}: `, err];
    errArr.push(thisErr);
    console.log(thisErr);
    await tx.rollback();
  }

  // connect to db > state table and store state object for next loop
  try {
    tx = await sequelize.transaction();
    await state_table
      .findOne({ where: { NAME: `${STATE_VAR_NAME}` }, tx })
      .then((entry, tx) => {
        // console.log('6: state object entry to be updated: ', entry);
        entry
          .update({ DATA: STATE }, { where: { NAME: `${STATE_VAR_NAME}` }, tx })
          .then((res, tx) =>
            console.log('5: state object entry update completed')
          );
      });
    await tx.commit();
  } catch (err) {
    thisErr = ['ERROR: saving updated state object: ', err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // close off all connections to db
  await sequelize.close();
  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    throw errArr;
  } else {
    return 'function completed';
  }
})();
