exports.handler = async function(event, context, callback) {
  /* ONLY FOR PROD */ context.callbackWaitsForEmptyEventLoop = false;

  // store ALL env vars for use throughout script
  // permanent
  const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE;
  const POSTGRES_USER = process.env.POSTGRES_USER;
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
  const POSTGRES_HOST = process.env.POSTGRES_HOST;
  // function specific
  const DB_TABLE_NAME = process.env.DB_TABLE_NAME;
  const STATE_VARS_TABLE_NAME = process.env.STATE_VARS_TABLE_NAME;
  const STATE_VAR_NAME = process.env.STATE_VAR_NAME;
  const N = process.env.DECIMAL_MAX_DIGITS;

  // import packages
  const axios = require('axios');
  const pg = require('pg');
  const Sequelize = require('sequelize');

  // initialize sequelize w db config
  const sequelize = new Sequelize(
    POSTGRES_DATABASE,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    {
      host: POSTGRES_HOST,
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        acquire: 10000,
        idle: 1000
      },
      operatorsAliases: false,
      logging: false
    }
  );

  // define state table schema
  const state_table = sequelize.define(STATE_VARS_TABLE_NAME, {
    UUID: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    NAME: Sequelize.STRING,
    DATA: Sequelize.JSON
  });

  // define data schema for binance klines table
  const table_schema = sequelize.define(DB_TABLE_NAME, {
    UUID: {
      type: Sequelize.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    SYMBOL: Sequelize.STRING,
    OPEN_TIME: Sequelize.INTEGER,
    OPEN: Sequelize.DECIMAL(N, 8),
    HIGH: Sequelize.DECIMAL(N, 8),
    LOW: Sequelize.DECIMAL(N, 8),
    CLOSE: Sequelize.DECIMAL(N, 8),
    VOLUME: Sequelize.DECIMAL(N, 8),
    CLOSE_TIME: Sequelize.INTEGER,
    QUOTE_ASSET_VOL: Sequelize.DECIMAL(N, 8),
    NUM_TRADES: Sequelize.INTEGER,
    TAKER_BUY_BASE_ASSET_VOL: Sequelize.DECIMAL(N, 8),
    TAKER_BUY_QUOTE_ASSET_VOL: Sequelize.DECIMAL(N, 8)
  });

  // fn state var
  let STATE;
  // sequelize async / await transaction pause var
  let tx;
  // error managing vars
  let errArr = [],
    thisErr = [];

  // connect to db > state table and retrieve state object for this loop
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await state_table.sync({ force: false }, tx);
    await state_table
      .findOrCreate({ where: { NAME: `${STATE_VAR_NAME}` }, tx })
      .spread((instance, created, tx) => {
        if (created) {
          console.log('1: state var created');
          STATE = undefined;
        } else {
          let stateVar = instance.get({ plain: true }, tx);
          console.log('1: state var retrieved from db / NOT created');
          STATE = stateVar.DATA;
        }
      });
    // console.log(`2: state object ${STATE_VAR_NAME}: ${STATE}`);
    await tx.commit();
  } catch (err) {
    thisErr = [
      'ERROR: collecting data from state table / parsing state object: ',
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // api response var
  let apiRes;

  // if 1st run or all symbols / pairs from last capture have been looped over
  if (
    !STATE ||
    (STATE && STATE.currentKey.i === Object.keys(STATE.apiData).length - 1)
  ) {
    try {
      console.log(
        '1st run or all symbols / pairs from last capture have been looped over'
      );
      // pull complete list of symbols from api
      apiRes = await axios.get(
        'https://api.binance.com/api/v1/ticker/allPrices'
      );
      // console.log("allPrices res object length: ", apiRes.data.length);
      // if 1st run
      if (!STATE) {
        // populate state object
        STATE = { currentKey: { s: undefined, i: undefined }, apiData: {} };
        // save all symbols to state object
        apiRes.data.forEach(entry => {
          STATE.apiData[entry.symbol] = { s: entry.symbol, t: 0 };
        });
        // console.log("New state object: ", STATE);
        // if end of loop over last symbol capture
      } else {
        // add any new symbols / pairs to persistent state object
        apiRes.data.forEach(entry => {
          STATE.apiData[entry.symbol]
            ? null
            : (STATE.apiData[entry.symbol] = { s: entry.symbol, t: 0 });
        });
        // and reset currentKey
        STATE.currentKey = { s: undefined, i: undefined };
        console.log('Updated state object: ', STATE);
      }
    } catch (err) {
      thisErr = [
        'ERROR: collecting data from API / creating state object: ',
        err
      ];
      errArr.push(thisErr);
      // console.log(thisErr);
    }
  }

  // update current key and symbol to next in line || 0
  STATE.currentKey.i =
    STATE.currentKey.i === undefined ? 0 : STATE.currentKey.i + 1;
  STATE.currentKey.s = Object.keys(STATE.apiData)[STATE.currentKey.i];
  console.log(
    '3: should contain symbol & object.keys index number :',
    STATE.currentKey
  );

  // setup this exe's api url
  const binance_url = `https://api.binance.com/api/v1/klines?symbol=${
    STATE.currentKey.s
  }&interval=1m`;

  // pull api kline data for this exe's symbol and save to db
  try {
    // pull klines for this symbol
    apiRes = await axios.get(binance_url);
    console.log(
      `4: klines interval=1m res object length: ${apiRes.data.length}`
    );
    // setup object to save data to db
    let formattedData = [];
    await apiRes.data.forEach(entry => {
      // if latest time saved for this symbol is less than this entries time, add to new object for db saving
      if (STATE.apiData[STATE.currentKey.s].t < entry[0]) {
        let newObject = {
          SYMBOL: STATE.currentKey.s,
          OPEN_TIME: entry[0] / 1000,
          OPEN: entry[1],
          HIGH: entry[2],
          LOW: entry[3],
          CLOSE: entry[4],
          VOLUME: entry[5],
          CLOSE_TIME: entry[6] / 1000,
          QUOTE_ASSET_VOL: entry[7],
          NUM_TRADES: entry[8],
          TAKER_BUY_BASE_ASSET_VOL: entry[9],
          TAKER_BUY_QUOTE_ASSET_VOL: entry[10]
        };
        formattedData.push(newObject);
      }
    });
    // store latest time captured for current symbol in state object
    STATE.apiData[STATE.currentKey.s].t =
      apiRes.data[apiRes.data.length - 1][0];
    // initialize sql transaction for saving to db with async / await
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await table_schema.sync({ force: false }, tx);
    await table_schema.bulkCreate(formattedData, tx).then((data, tx) => {
      console.log(
        `5: New klines interval=1m data successfully saved to db: ${DB_TABLE_NAME}`
      );
    });
    await tx.commit();
  } catch (err) {
    thisErr = [
      'ERROR: with klines interval=1m API / creating new data object: ',
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // connect to db > state table and store state object for next loop
  try {
    tx = await sequelize.transaction();
    await state_table
      .findOne({ where: { NAME: `${STATE_VAR_NAME}` }, tx })
      .then((entry, tx) => {
        console.log('6: state object entry to be updated: ', entry);
        entry
          .update({ DATA: STATE }, { where: { NAME: `${STATE_VAR_NAME}` }, tx })
          .then((res, tx) =>
            console.log('7: state object entry update completed')
          );
      });
    await tx.commit();
  } catch (err) {
    thisErr = ['ERROR: saving updated state object: ', err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // close off all connections to db
  await sequelize.close();
  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    // throw errArr;
    callback(errArr, null);
  } else {
    // return 'function completed';
    callback(null, 'function completed');
  }
};
