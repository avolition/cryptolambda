const data = require('./test.data.js');
const { performance } = require('perf_hooks');
const numeral = require('numeral');

console.log(performance);

let total = 0;

const iterations = 1;

for (let i = 0; i < iterations; i++) {
  performance.mark(`loop ${i} start`);

  let formattedData = [];

  for (const entry of data) {
    // create new state obj for this entry
    // const e = {
    //   SYMBOL: entry.symbol,
    //   PRICE_CHG: numeral(entry.priceChange)._value,
    //   PRICE_CHG_PERCENT: numeral(entry.priceChangePercent)._value,
    //   WEIGHTED_AVG_PRICE: numeral(entry.weightedAvgPrice)._value,
    //   PREV_CLOSE_PRICE: numeral(entry.prevClosePrice)._value,
    //   LAST_PRICE: numeral(entry.lastPrice)._value,
    //   LAST_QTY: numeral(entry.lastQty)._value,
    //   BID_PRICE: numeral(entry.bidPrice)._value,
    //   BID_QTY: numeral(entry.bidQty)._value,
    //   ASK_PRICE: numeral(entry.askPrice)._value,
    //   ASK_QTY: numeral(entry.askQty)._value,
    //   OPEN_PRICE: numeral(entry.openPrice)._value,
    //   HIGH_PRICE: numeral(entry.highPrice)._value,
    //   LOW_PRICE: numeral(entry.lowPrice)._value,
    //   VOLUME: numeral(entry.volume)._value,
    //   QUOTE_VOLUME: numeral(entry.quoteVolume)._value,
    //   OPEN_TIME: numeral(entry.openTime)._value,
    //   CLOSE_TIME: numeral(entry.closeTime)._value,
    //   FIRST_ID: numeral(entry.firstId)._value,
    //   LAST_ID: numeral(entry.lastId)._value,
    //   COUNT: numeral(entry.count)._value
    // };
    // 1x iterations average time === 55ms
    // 10x iterations average time === 175ms
    // 100x iterations average time === 1000ms
    // 1000x iterations average time === 8358ms

    const f = {
      SYMBOL: entry.symbol,
      PRICE_CHG: entry.priceChange,
      PRICE_CHG_PERCENT: entry.priceChangePercent,
      WEIGHTED_AVG_PRICE: entry.weightedAvgPrice,
      PREV_CLOSE_PRICE: entry.prevClosePrice,
      LAST_PRICE: entry.lastPrice,
      LAST_QTY: entry.lastQty,
      BID_PRICE: entry.bidPrice,
      BID_QTY: entry.bidQty,
      ASK_PRICE: entry.askPrice,
      ASK_QTY: entry.askQty,
      OPEN_PRICE: entry.openPrice,
      HIGH_PRICE: entry.highPrice,
      LOW_PRICE: entry.lowPrice,
      VOLUME: entry.volume,
      QUOTE_VOLUME: entry.quoteVolume,
      OPEN_TIME: entry.openTime,
      CLOSE_TIME: entry.closeTime,
      FIRST_ID: entry.firstId,
      LAST_ID: entry.lastId,
      COUNT: entry.count
    };
    // 1x iterations average time === 40ms
    // 10x iterations average time === 71ms
    // 100x iterations average time === 90ms
    // 1000x iterations average time === 111ms

    // const ej = JSON.stringify(e);
    // const fj = JSON.stringify(f);
    // if (ej === fj) console.log('madman ', ej, fj);

    // formattedData.push(e);
    formattedData.push(f);
  }

  performance.mark(`loop ${i} end`);
  performance.measure('Inputs validation', `loop ${i} start`, `loop ${i} end`);
  total += performance.nodeTiming.duration || 0;
}
console.log(Math.round(total / (iterations + 1)));

// let total2 = 0;

// for (let i = 0; i < iterations; i++) {
//   performance.mark(`loop ${i} start`);

//   const STATE = data.reduce((agg, pair) => {
//     STATE[pair] = { s: pair, hist: [] };
//   });

//   performance.mark(`loop ${i} end`);
//   performance.measure('Inputs validation', `loop ${i} start`, `loop ${i} end`);
//   const measurements = performance.getEntriesByType('measure');
//   total2 += measurements[0].duration || 0;
// }
// console.log(total2 / (iterations + 1));
