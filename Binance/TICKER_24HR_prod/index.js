exports.handler = async function(event, context, callback) {
  /* ONLY FOR PROD */ context.callbackWaitsForEmptyEventLoop = false;

  // store ALL env vars for use throughout script
  // permanent
  const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE;
  const POSTGRES_USER = process.env.POSTGRES_USER;
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
  const POSTGRES_HOST = process.env.POSTGRES_HOST;
  // function specific
  const DB_TABLE_NAME = process.env.DB_TABLE_NAME;
  const N = process.env.DECIMAL_MAX_DIGITS;
  const STATE_VARS_TABLE_NAME = process.env.STATE_VARS_TABLE_NAME;
  const STATE_VAR_NAME = process.env.STATE_VAR_NAME;
  const TELEGRAM_PAIRS = process.env.TELEGRAM_NOTIFICATION_PAIRS.split(',');
  const HIST_INDEX_1 = parseInt(process.env.HIST_INDEX_1);
  const HIST_PERCENT_1 = parseFloat(process.env.HIST_PERCENT_1).toFixed(2);
  const HIST_INDEX_2 = parseInt(process.env.HIST_INDEX_2);
  const HIST_PERCENT_2 = parseFloat(process.env.HIST_PERCENT_2).toFixed(2);
  const HIST_INDEX_3 = parseInt(process.env.HIST_INDEX_3);
  const HIST_PERCENT_3 = parseFloat(process.env.HIST_PERCENT_3).toFixed(2);
  const HIST_MAX_LENGTH = process.env.HIST_MAX_LENGTH;
  const TELEGRAM_BOT_TOKEN = process.env.TELEGRAM_BOT_TOKEN;
  const TELEGRAM_CHAT_ID = process.env.TELEGRAM_CHAT_ID; // -310814116 for production
  const TELEGRAM_ENABLED = process.env.TELEGRAM_ENABLED;

  // eval(require('locus'));

  // import packages
  const axios = require('axios');
  const pg = require('pg');
  const Sequelize = require('sequelize');

  // initialize sequelize w db config
  const sequelize = new Sequelize(
    POSTGRES_DATABASE,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    {
      host: POSTGRES_HOST,
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        acquire: 10000,
        idle: 1000
      },
      operatorsAliases: false,
      logging: false // prevent SQL query results printing to console
    }
  );

  // define state table schema
  const state_table = sequelize.define(STATE_VARS_TABLE_NAME, {
    UUID: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    NAME: Sequelize.STRING,
    DATA: Sequelize.JSON
  });

  // define data schema for binance klines table
  const table_schema = sequelize.define(DB_TABLE_NAME, {
    UUID: {
      type: Sequelize.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    SYMBOL: Sequelize.STRING,
    PRICE_CHG: Sequelize.DECIMAL(N, 8),
    PRICE_CHG_PERCENT: Sequelize.DECIMAL(N, 8),
    WEIGHTED_AVG_PRICE: Sequelize.DECIMAL(N, 8),
    PREV_CLOSE_PRICE: Sequelize.DECIMAL(N, 8),
    LAST_PRICE: Sequelize.DECIMAL(N, 8),
    LAST_QTY: Sequelize.DECIMAL(N, 8),
    BID_PRICE: Sequelize.DECIMAL(N, 8),
    BID_QTY: Sequelize.DECIMAL(N, 8),
    ASK_PRICE: Sequelize.DECIMAL(N, 8),
    ASK_QTY: Sequelize.DECIMAL(N, 8),
    OPEN_PRICE: Sequelize.DECIMAL(N, 8),
    HIGH_PRICE: Sequelize.DECIMAL(N, 8),
    LOW_PRICE: Sequelize.DECIMAL(N, 8),
    VOLUME: Sequelize.DECIMAL(N, 8),
    QUOTE_VOLUME: Sequelize.DECIMAL(N, 8),
    OPEN_TIME: Sequelize.BIGINT,
    CLOSE_TIME: Sequelize.BIGINT,
    FIRST_ID: Sequelize.INTEGER,
    LAST_ID: Sequelize.INTEGER,
    COUNT: Sequelize.INTEGER
  });

  // fn state var
  let STATE;
  // sequelize async / await transaction pause var
  let tx;
  // error var
  let errArr = [],
    thisErr = [];

  // connect to db > state table and retrieve state object for this loop
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await state_table.sync({ force: false }, tx);
    await state_table
      .findOrCreate({ where: { NAME: `${STATE_VAR_NAME}` }, tx })
      .spread((instance, created, tx) => {
        if (created) {
          console.log('1: state var created');
          STATE = undefined;
        } else {
          let stateVar = instance.get({ plain: true }, tx);
          console.log('1: state var retrieved from db / NOT created');
          STATE = stateVar.DATA;
        }
      });
    // console.log(`2: state object ${STATE_VAR_NAME}: ${STATE}`);
    await tx.commit();
  } catch (err) {
    thisErr = [
      'ERROR: collecting data from state table / parsing state object: ',
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // if STATE doesn't exist yet, populate pairs for monitoring from env var
  if (!STATE) {
    STATE = {};
    TELEGRAM_PAIRS.forEach(pair => {
      STATE[pair] = { s: pair, hist: [] };
    });
    console.log(`3: Created STATE object: `, STATE);
  }

  // if new pair(s) to monitor added to env var, add to STATE for monitoring
  if (TELEGRAM_PAIRS.length > Object.keys(STATE).length) {
    for (let pair of TELEGRAM_PAIRS) {
      if (!STATE[pair]) {
        STATE[pair] = { s: pair, hist: [] };
        console.log(
          `Added new pair ${pair} to STATE as added to TELEGRAM_NOTIFICATION_PAIRS for monitoring`
        );
      }
    }
  }

  // if pair(s) removed from monitor env var, remove from STATE
  if (TELEGRAM_PAIRS.length < Object.keys(STATE).length) {
    for (let pair of Object.keys(STATE)) {
      if (!TELEGRAM_PAIRS.includes(pair)) {
        delete STATE[pair];
        console.log(
          `Deleted pair ${pair} from STATE as removed from TELEGRAM_NOTIFICATION_PAIRS}`
        );
      }
    }
  }

  // api response var
  let apiRes;
  // var for storing formatted api data;
  let formattedData = [];
  // telegram url for sending notifications
  const TELEGRAM_CHAT_URL = `https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage?chat_id=${TELEGRAM_CHAT_ID}&text=`;

  // pull api ticker/24hr data for this exe's symbol and save to db
  try {
    // pull api ticker/24hr for this symbol
    apiRes = await axios.get('https://api.binance.com/api/v1/ticker/24hr');
    console.log(`4: /ticker/24hr res object length: ${apiRes.data.length}`);
    for (let entry of apiRes.data) {
      // create new state obj for this entry
      let e = {
        SYMBOL: entry.symbol,
        PRICE_CHG: entry.priceChange,
        PRICE_CHG_PERCENT: entry.priceChangePercent,
        WEIGHTED_AVG_PRICE: entry.weightedAvgPrice,
        PREV_CLOSE_PRICE: entry.prevClosePrice,
        LAST_PRICE: entry.lastPrice,
        LAST_QTY: entry.lastQty,
        BID_PRICE: entry.bidPrice,
        BID_QTY: entry.bidQty,
        ASK_PRICE: entry.askPrice,
        ASK_QTY: entry.askQty,
        OPEN_PRICE: entry.openPrice,
        HIGH_PRICE: entry.highPrice,
        LOW_PRICE: entry.lowPrice,
        VOLUME: entry.volume,
        QUOTE_VOLUME: entry.quoteVolume,
        OPEN_TIME: entry.openTime,
        CLOSE_TIME: entry.closeTime,
        FIRST_ID: entry.firstId,
        LAST_ID: entry.lastId,
        COUNT: entry.count
      };
      // check if monitoring pair & if change in last x mins should trigger telegram notification
      // if monitoring symbol / pair, populate data for testing
      if (STATE[e.SYMBOL]) {
        let lp = e.LAST_PRICE;
        if (
          TELEGRAM_ENABLED &&
          lp != Infinity &&
          lp != NaN &&
          typeof lp === 'number' &&
          lp > 0
        ) {
          // console.log(`${e.SYMBOL} exists in STATE - adding LAST_PRICE: ${e.LAST_PRICE}`);
          STATE[e.SYMBOL].hist.unshift(e.LAST_PRICE);
          let hist = STATE[e.SYMBOL].hist;
          // console.log(`LAST_PRICE added: ${hist[0]}`);
          // console.log(`${STATE[e.SYMBOL]} hist array: ${hist}`);
          // if long enough for comparison, test if index[0] against HIST_INDEX_1 >= HIST_PERCENT_1
          if (hist.length > HIST_INDEX_1) {
            let result1 = (100 / hist[HIST_INDEX_1]) * hist[0] - 100;
            // console.log(`result1: ${result1.toFixed(2)}`);
            if (Math.abs(result1) >= HIST_PERCENT_1) {
              // console.log(
              //   `TELEGRAM: ${e.SYMBOL} changed ${result1.toFixed(
              //     2
              //   )}% in the last ${HIST_INDEX_1} minute`
              // );
              let res = await axios.get(
                `${TELEGRAM_CHAT_URL}+${e.SYMBOL}+changed+${result1.toFixed(
                  2
                )}%+in+${HIST_INDEX_1}+minute+on+Binance`
              );
              // console.log(`response1 from TELEGRAM: ${res}`);
            }
            // if length = HIST_INDEX_2, test if index[0] against HIST_INDEX_2 >= HIST_PERCENT_2
            if (hist.length > HIST_INDEX_2) {
              let result2 = (100 / hist[HIST_INDEX_2]) * hist[0] - 100;
              // console.log(`inputs: HIST_INDEX_2 - STATE[e.SYMBOL].hist[HIST_INDEX_2]`);
              // console.log(`result2: ${result2.toFixed(2)}`);
              if (Math.abs(result2) >= HIST_PERCENT_2) {
                // console.log(
                //   `TELEGRAM: ${e.SYMBOL} changed ${result2.toFixed(
                //     2
                //   )}% in the last ${HIST_INDEX_2} minutes`
                // );
                let res = await axios.get(
                  `${TELEGRAM_CHAT_URL}+${e.SYMBOL}+changed+${result2.toFixed(
                    2
                  )}%+in+${HIST_INDEX_2}+minutes+on+Binance`
                );
                // console.log(`response2 from TELEGRAM: ${res}`);
              }
            }
            // if long enough for comparison, test if index[0] against HIST_INDEX_3 >= HIST_PERCENT_3
            if (hist.length > HIST_INDEX_3) {
              let result3 = (100 / hist[HIST_INDEX_3]) * hist[0] - 100;
              // console.log(`result3: ${result3.toFixed(2)}`);
              if (Math.abs(result3) >= HIST_PERCENT_3) {
                // console.log(
                //   `TELEGRAM: ${e.SYMBOL} changed ${result3.toFixed(
                //     2
                //   )}% in the last ${HIST_INDEX_3} minutes`
                // );
                let res = await axios.get(
                  `${TELEGRAM_CHAT_URL}+${e.SYMBOL}+changed+${result3.toFixed(
                    2
                  )}%+in+${HIST_INDEX_3}+minutes+on+Binance`
                );
                // console.log(`response3 from TELEGRAM: ${res}`);
              }
            }
            // if greater than max length, remove last array item
            if (hist.length > HIST_MAX_LENGTH) STATE[e.SYMBOL].hist.pop();
          }
        }
      }
      // add to formatted data array
      formattedData.push(e);
    }
  } catch (err) {
    thisErr = ['ERROR: collecting data from API / creating new object: ', err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // save new data to db
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await table_schema.sync({ force: false }, tx);
    await table_schema.bulkCreate(formattedData, tx).then((data, tx) => {
      console.log(
        `5: New /ticker/24hr data successfully saved to db: ${DB_TABLE_NAME}`
      );
    });
    await tx.commit();
  } catch (err) {
    thisErr = [
      `ERROR: saving new /ticker/24hr object to db: ${DB_TABLE_NAME}`,
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // connect to db > state table and store state object for next loop
  try {
    tx = await sequelize.transaction();
    await state_table
      .findOne({ where: { NAME: `${STATE_VAR_NAME}` }, tx })
      .then((entry, tx) => {
        console.log('6: state object entry to be updated');
        entry
          .update({ DATA: STATE }, { where: { NAME: `${STATE_VAR_NAME}` }, tx })
          .then((res, tx) =>
            console.log('7: state object entry update completed')
          );
      });
    await tx.commit();
  } catch (err) {
    thisErr = ['ERROR: saving updated state object: ', err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // close off all connections to db
  await sequelize.close();
  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    // throw errArr;
    callback(errArr, null);
  } else {
    // return 'function completed';
    callback(null, 'function completed');
  }
};
