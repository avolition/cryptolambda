(async function() {
  /* NOT FOR PROD */ require('dotenv').config({ path: 'variables.env' });

  // store ALL env vars for use throughout script
  // permanent
  const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE;
  const POSTGRES_USER = process.env.POSTGRES_USER;
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
  const POSTGRES_HOST = process.env.POSTGRES_HOST;
  // function specific
  const DB_TABLE_NAME = process.env.DB_TABLE_NAME;
  const N = process.env.DECIMAL_MAX_DIGITS;

  // import packages
  const axios = require('axios');
  const pg = require('pg');
  const Sequelize = require('sequelize');

  // initialize sequelize w db config
  const sequelize = new Sequelize(
    POSTGRES_DATABASE,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    {
      host: POSTGRES_HOST,
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        acquire: 10000,
        idle: 1000
      },
      operatorsAliases: false,
      logging: false
    }
  );

  // define table schema for this api
  const table_schema = sequelize.define(DB_TABLE_NAME, {
    UUID: {
      type: Sequelize.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    SYMBOL: Sequelize.STRING,
    LAST: Sequelize.DECIMAL(N, 8),
    LOWEST_ASK: Sequelize.DECIMAL(N, 8),
    HIGHEST_BID: Sequelize.DECIMAL(N, 8),
    PERCENT_CHG: Sequelize.DECIMAL(N, 8),
    BASE_VOL: Sequelize.DECIMAL(N, 8),
    QUOTE_VOL: Sequelize.DECIMAL(N, 8),
    IS_FROZEN: Sequelize.BOOLEAN,
    HIGH_24HR: Sequelize.DECIMAL(N, 8),
    LOW_24HR: Sequelize.DECIMAL(N, 8),
    L_SYMBOL: Sequelize.STRING,
    L_SYMBOL_VOL_24HR: Sequelize.DECIMAL(N, 8),
    R_SYMBOL: Sequelize.STRING,
    R_SYMBOL_VOL_24HR: Sequelize.DECIMAL(N, 8),
    TIMESTAMP: Sequelize.INTEGER
  });

  // api response var
  let apiRes1, apiRes2;
  // var for storing formatted api data;
  let formattedData = [];
  // error handling vars
  let errArr = [],
    thisErr = [];

  // pull api data and format for saving to db
  try {
    apiRes1 = await axios.get(
      `https://poloniex.com/public?command=returnTicker`
    );
    apiRes2 = await axios.get(
      `https://poloniex.com/public?command=return24hVolume`
    );
    const time = Math.round(Date.now() / 1000);
    formattedData = Object.keys(apiRes1.data).map(entry => {
      let thisPairT = apiRes1.data[entry];
      let thisPairV = apiRes2.data[entry];
      return {
        SYMBOL: entry,
        LAST: thisPairT.last,
        LOWEST_ASK: thisPairT.lowestAsk,
        HIGHEST_BID: thisPairT.highestBid,
        PERCENT_CHG: thisPairT.percentChange,
        BASE_VOL: thisPairT.baseVolume,
        QUOTE_VOL: thisPairT.quoteVolume,
        IS_FROZEN: thisPairT.isFrozen,
        HIGH_24HR: thisPairT.high24hr,
        LOW_24HR: thisPairT.low24hr,
        L_SYMBOL: Object.entries(thisPairV)[0][0],
        L_SYMBOL_VOL_24HR: Object.entries(thisPairV)[0][1],
        R_SYMBOL: Object.entries(thisPairV)[1][0],
        R_SYMBOL_VOL_24HR: Object.entries(thisPairV)[1][1],
        TIMESTAMP: time
      };
    });
  } catch (err) {
    thisErr = [
      `ERROR: retrieving data from api / creating formattedData object: `,
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
  }

  // sequelize async / await enabler var
  let tx;

  // save data to DB
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await table_schema.sync({ force: false }, tx);
    await table_schema.bulkCreate(formattedData, tx).then((data, tx) => {
      console.log(
        `New Ticker / 24hr Volume data successfully saved to db ${DB_TABLE_NAME}`
      );
    });
    await tx.commit();
  } catch (err) {
    thisErr = [`ERROR: saving new data to db ${DB_TABLE_NAME}: `, err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // close off all connections to db
  await sequelize.close();
  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    throw errArr;
  } else {
    return 'function completed';
  }
})();
