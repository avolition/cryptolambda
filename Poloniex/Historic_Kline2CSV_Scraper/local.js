(async function () {

  // import packages
  const axios = require("axios");
  const json2csv = require("json2csv");
  const fs = require("fs");

  // list of all Poloniex trading pairs as of 01/02/2018
  // `BTC_BCN,BTC_BELA,BTC_BLK,BTC_BTCD,BTC_BTM,BTC_BTS,BTC_BURST,BTC_CLAM,BTC_DASH,BTC_DGB,BTC_DOGE,BTC_EMC2,BTC_FLDC,BTC_FLO,BTC_GAME,BTC_GRC,BTC_HUC,BTC_LTC,BTC_MAID,BTC_OMNI,BTC_NAV,BTC_NEOS,BTC_NMC,BTC_NXT,BTC_PINK,BTC_POT,BTC_PPC,BTC_RIC,BTC_STR,BTC_SYS,BTC_VIA,BTC_XVC,BTC_VRC,BTC_VTC,BTC_XBC,BTC_XCP,BTC_XEM,BTC_XMR,BTC_XPM,BTC_XRP,USDT_BTC,USDT_DASH,USDT_LTC,USDT_NXT,USDT_STR,USDT_XMR,USDT_XRP,XMR_BCN,XMR_BLK,XMR_BTCD,XMR_DASH,XMR_LTC,XMR_MAID,XMR_NXT,BTC_ETH,USDT_ETH,BTC_SC,BTC_BCY,BTC_EXP,BTC_FCT,BTC_RADS,BTC_AMP,BTC_DCR,BTC_LSK,ETH_LSK,BTC_LBC,BTC_STEEM,ETH_STEEM,BTC_SBD,BTC_ETC,ETH_ETC,USDT_ETC,BTC_REP,USDT_REP,ETH_REP,BTC_ARDR,BTC_ZEC,ETH_ZEC,USDT_ZEC,XMR_ZEC,BTC_STRAT,BTC_NXC,BTC_PASC,BTC_GNT,ETH_GNT,BTC_GNO,ETH_GNO,BTC_BCH,ETH_BCH,USDT_BCH,BTC_ZRX,ETH_ZRX,BTC_CVC,ETH_CVC,BTC_OMG,ETH_OMG,BTC_GAS,ETH_GAS,BTC_STORJ`
  let completedSymbols = [];
  let allSymbols = `BTC_BCN,BTC_BELA,BTC_BLK,BTC_BTCD,BTC_BTM,BTC_BTS,BTC_BURST,BTC_CLAM,BTC_DASH,BTC_DGB,BTC_DOGE,BTC_EMC2,BTC_FLDC,BTC_FLO,BTC_GAME,BTC_GRC,BTC_HUC,BTC_LTC,BTC_MAID,BTC_OMNI,BTC_NAV,BTC_NEOS,BTC_NMC,BTC_NXT,BTC_PINK,BTC_POT,BTC_PPC,BTC_RIC,BTC_STR,BTC_SYS,BTC_VIA,BTC_XVC,BTC_VRC,BTC_VTC,BTC_XBC,BTC_XCP,BTC_XEM,BTC_XMR,BTC_XPM,BTC_XRP,USDT_BTC,USDT_DASH,USDT_LTC,USDT_NXT,USDT_STR,USDT_XMR,USDT_XRP,XMR_BCN,XMR_BLK,XMR_BTCD,XMR_DASH,XMR_LTC,XMR_MAID,XMR_NXT,BTC_ETH,USDT_ETH,BTC_SC,BTC_BCY,BTC_EXP,BTC_FCT,BTC_RADS,BTC_AMP,BTC_DCR,BTC_LSK,ETH_LSK,BTC_LBC,BTC_STEEM,ETH_STEEM,BTC_SBD,BTC_ETC,ETH_ETC,USDT_ETC,BTC_REP,USDT_REP,ETH_REP,BTC_ARDR,BTC_ZEC,ETH_ZEC,USDT_ZEC,XMR_ZEC,BTC_STRAT,BTC_NXC,BTC_PASC,BTC_GNT,ETH_GNT,BTC_GNO,ETH_GNO,BTC_BCH,ETH_BCH,USDT_BCH,BTC_ZRX,ETH_ZRX,BTC_CVC,ETH_CVC,BTC_OMG,ETH_OMG,BTC_GAS,ETH_GAS,BTC_STORJ`.split(',');

  // import test data
  // const testData = require("./testData");
  // const testData1 = require("./testData1");
  // const testData2 = require("./testData2");

  // eval(require('locus'));

  // error handling vars
  let errArr = [], thisErr = [];
  // setup data
  let apiRes = {}

  // fn for delaying api calls to prevent server error / rejection
  const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

  // timestamps for setting / updating api calls until all Historic data collected
  const TIMESTAMP_START = 0000000001;
  const TIMESTAMP_END = 1517443500;
  const period = 86400;

  // pull api data and format for saving to csv
  try {
    for (let symbol of allSymbols) {
      // api response var
      let apiData = {};
      // var for storing formatted api data;
      let formattedData = [];
      let repeat = true;
      const time = Date.now();
      // while loop for re-pinging api on error
      while (repeat) {
        try {
          console.log(`START - loop for ${symbol} started @ ${Date(time).toString()}`);
          console.log(`1: start api request for ${symbol} @ ${Date().toString()}`);
          let url = `https://poloniex.com/public?command=returnChartData&currencyPair=${symbol}&start=${TIMESTAMP_START}&end=${TIMESTAMP_END}&period=${period}`;
          console.log(`pinging ${url}`);
          apiRes = await axios.get(url); // dynamic
          apiData = apiRes.data;
          console.log(`2: end api request for ${symbol} @ ${Date().toString()}`);
          repeat = false;
        } catch (err) {
          if (err.response.status === 500) {
            console.log(`DUE TO ERROR: waiting 1 min before next trying api`);
            await delay(60000);
          }
        }
      }
      console.log(`3: start formattedData loop for ${symbol} @ ${Date().toString()
      }`);
      for (let entry of apiData) {
        // eval(require('locus'));
        // console.log(`entry date: ${entry.date}`);
        formattedData.push({
          SYMBOL: symbol,
          DATE: entry.date ? new Date(entry.date * 1000).toISOString() : null,
          TIMESTAMP: entry.date,
          HIGH: entry.high,
          LOW: entry.low,
          OPEN: entry.open,
          CLOSE: entry.close,
          VOLUME: entry.volume,
          QUOTE_VOL: entry.quoteVolume,
          WEIGHTED_AVG: entry.weightedAverage
        });
      };
      console.log(`4: end formattedData loop for ${symbol} @ ${Date().toString()}`);
      console.log(`5: formattedData length ${formattedData.length}`);
      console.log(`6: formattedData start entry ${formattedData[0].TIMESTAMP}`);
      console.log(`7: formattedData end entry ${formattedData[formattedData.length - 1].TIMESTAMP}`);
      console.log(`8: end api request for ${symbol} @ ${Date().toString()}`);
      console.log(`9: start conversion to csv for ${symbol} @ ${Date().toString()}`);
      // options for formatting csv
      const opts = {
        data: formattedData,
        fields: ['SYMBOL', 'DATE', 'TIMESTAMP', 'HIGH', 'LOW', 'OPEN', 'CLOSE', 'VOLUME', 'QUOTE_VOL', 'WEIGHTED_AVG'],
        quotes: ''
      };
      const csv = await json2csv(opts);
      console.log(`10: end conversion to csv for ${symbol} @ ${Date().toString()}`);
      console.log(`11: start writeFile for ${symbol} @ ${Date().toString()}`);
      fs.writeFile(`./data/AllTimeHistoric/AllTimeHistoric_Kline24hr_${symbol}.csv`, csv, function(err) {
        if (err) throw err;
        console.log(`12: /data/AllTimeHistoric/AllTimeHistoric_Kline24hr_${symbol}.csv successfully saved @ ${Date().toString()}`);
      });
      console.log(`13: end writeFile / loop for ${symbol} @ ${Date().toString()}`);
      console.log(`14: loop for ${symbol} took ${Date.now() - time} milliseconds`);
      console.log(`END - loop for ${symbol} ended @ ${Date(Date.now()).toString()} milliseconds`);
      completedSymbols.push(symbol);
      // setup delay loop so I can cancel between loops if necessary
      const delayNextLoopTimeSeconds = 30;
      let delayTimeArr = [];
      for (let i = 0, j = delayNextLoopTimeSeconds ; i = j ; j-- ) {
        delayTimeArr.push(j);
      }
      for (let sec of delayTimeArr) {
        if (sec % 10 === 0) console.log(`seconds until next loop starts - ${sec}`);
        await delay(1000);
      }
    }
  } catch (err) {
    thisErr = [`ERROR: retrieving data from api / creating formattedData object: `, err];
    errArr.push(thisErr);
    console.log(thisErr);
  }

  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    console.log(errArr);
  } else { 
    console.log("function completed");
    console.log(`Final completedSymbols: ${completedSymbols}`);
    console.log(`Final allSymbols: ${allSymbols}`);
  };

})();