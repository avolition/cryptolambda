(async function() {
  /* NOT FOR PROD */ require('dotenv').config({ path: 'variables.env' });

  // store ALL env vars for use throughout script
  // permanent
  const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE;
  const POSTGRES_USER = process.env.POSTGRES_USER;
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
  const POSTGRES_HOST = process.env.POSTGRES_HOST;
  // function specific
  const DB_TABLE_NAME = process.env.DB_TABLE_NAME;
  const N = process.env.DECIMAL_MAX_DIGITS;

  // import packages
  const axios = require('axios');
  const pg = require('pg');
  const Sequelize = require('sequelize');

  // initialize sequelize w db config
  const sequelize = new Sequelize(
    POSTGRES_DATABASE,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    {
      host: POSTGRES_HOST,
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        acquire: 10000,
        idle: 1000
      },
      operatorsAliases: false,
      logging: false
    }
  );

  // define table schema for this api
  const table_schema = sequelize.define(DB_TABLE_NAME, {
    UUID: {
      type: Sequelize.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    SYMBOL: Sequelize.STRING,
    BID: Sequelize.DECIMAL(N, 8),
    BID_SIZE: Sequelize.DECIMAL(N, 8),
    ASK: Sequelize.DECIMAL(N, 8),
    ASK_SIZE: Sequelize.DECIMAL(N, 8),
    DAILY_CHANGE: Sequelize.DECIMAL(N, 8),
    DAILY_CHANGE_PERC: Sequelize.DECIMAL(N, 8),
    LAST_PRICE: Sequelize.DECIMAL(N, 8),
    VOLUME: Sequelize.DECIMAL(N, 8),
    HIGH: Sequelize.DECIMAL(N, 8),
    LOW: Sequelize.DECIMAL(N, 8)
  });

  // sequelize async / await enabler var
  let tx;
  // error handling vars
  let errArr = [],
    thisErr = [];

  // api response var
  let apiRes;
  // var for storing formatted api data;
  let formattedData = [];
  // pull api ticker/24hr data for this exe's symbol and save to db
  try {
    // pull api ticker/24hr for this symbol
    apiRes = await axios.get('https://api.bitfinex.com/v2/tickers?symbols=ALL');
    console.log(`4: /tickers/24hr res object length: ${apiRes.data.length}`);
    for (let entry of apiRes.data) {
      // create new state obj for this entry
      if (entry.length === 11) {
        const e = {
          SYMBOL: entry[0],
          BID: entry[1],
          BID_SIZE: entry[2],
          ASK: entry[3],
          ASK_SIZE: entry[4],
          DAILY_CHANGE: entry[5],
          DAILY_CHANGE_PERC: entry[6],
          LAST_PRICE: entry[7],
          VOLUME: entry[8],
          HIGH: entry[9],
          LOW: entry[10]
        };
        formattedData.push(e);
      }
    }
  } catch (err) {
    thisErr = [`ERROR: with Bitfinex ticker: `, err];
    errArr.push(thisErr);
    // console.log(thisErr);
  }

  // save data to DB
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await table_schema.sync({ force: false }, tx);
    await table_schema.bulkCreate(formattedData, tx).then((data, tx) => {
      console.log(
        '4: New ticker data successfully saved to db: ',
        DB_TABLE_NAME
      );
    });
    await tx.commit();
  } catch (err) {
    thisErr = [
      `ERROR: saving ${rollbackArray} symbols to ${DB_TABLE_NAME} db: `,
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // close off all connections to db
  await sequelize.close();
  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    throw errArr;
  } else {
    return 'function completed';
  }
})();
