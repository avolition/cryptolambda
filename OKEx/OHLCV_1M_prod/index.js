exports.handler = async function(event, context, callback) {
  /* ONLY FOR PROD */ context.callbackWaitsForEmptyEventLoop = false;

  // store ALL env vars for use throughout script
  // permanent
  const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE;
  const POSTGRES_USER = process.env.POSTGRES_USER;
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
  const POSTGRES_HOST = process.env.POSTGRES_HOST;
  // function specific
  const DB_TABLE_NAME = process.env.DB_TABLE_NAME;
  const N = process.env.DECIMAL_MAX_DIGITS;
  const STATE_VARS_TABLE_NAME = process.env.STATE_VARS_TABLE_NAME;
  const STATE_VAR_NAME = process.env.STATE_VAR_NAME;
  const EXCHANGE_SYMBOLS = process.env.EXCHANGE_SYMBOLS.split(',');

  // import packages
  const axios = require('axios');
  const pg = require('pg');
  const Sequelize = require('sequelize');

  // initialize sequelize w db config
  const sequelize = new Sequelize(
    POSTGRES_DATABASE,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    {
      host: POSTGRES_HOST,
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        acquire: 10000,
        idle: 1000
      },
      operatorsAliases: false,
      logging: false
    }
  );

  // define state table schema
  const state_table = sequelize.define(STATE_VARS_TABLE_NAME, {
    UUID: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    NAME: Sequelize.STRING,
    DATA: Sequelize.JSON
  });

  // define table schema for this api
  const table_schema = sequelize.define(DB_TABLE_NAME, {
    UUID: {
      type: Sequelize.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    SYMBOL: Sequelize.STRING,
    OPEN_TIME: Sequelize.INTEGER,
    OPEN: Sequelize.DECIMAL(N, 8),
    HIGH: Sequelize.DECIMAL(N, 8),
    LOW: Sequelize.DECIMAL(N, 8),
    CLOSE: Sequelize.DECIMAL(N, 8),
    VOLUME: Sequelize.DECIMAL(N, 8)
  });

  // fn state var
  let STATE;
  // sequelize async / await transaction pause var
  let tx;
  // error managing vars
  let errArr = [],
    thisErr = [];

  // connect to db > state table and retrieve state object for this loop
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await state_table.sync({ force: false }, tx);
    await state_table
      .findOrCreate({ where: { NAME: `${STATE_VAR_NAME}` }, tx })
      .spread((instance, created, tx) => {
        if (created) {
          console.log('1: state var created');
          STATE = undefined;
        } else {
          let stateVar = instance.get({ plain: true }, tx);
          console.log('1: state var retrieved from db / NOT created');
          STATE = stateVar.DATA;
        }
      });
    // console.log(`2: state object ${STATE_VAR_NAME}: ${STATE}`);
    await tx.commit();
  } catch (err) {
    thisErr = [
      'ERROR: collecting data from state table / parsing state object: ',
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  if (!STATE) {
    STATE = {};
    // save all symbols to state object
    EXCHANGE_SYMBOLS.forEach(symbol => {
      STATE[symbol] = { t: 0 };
    });
  }

  // var for storing formatted api data;
  let formattedData = [],
    i = 1;

  // pull api data and format for saving to db
  try {
    for (let symbol of EXCHANGE_SYMBOLS) {
      const url = `https://www.okex.com/api/v1/kline.do?symbol=${symbol}&type=1min`;
      console.log(`3.${i}: GETting symbol from ${url} @ ${Date.now()}`);
      let apiRes = await axios.get(url);
      apiRes.data.forEach(entry => {
        // if latest time saved for this symbol is less than this entries time, add to new object for db saving
        if (STATE[symbol].t < entry[0]) {
          formattedData.push({
            SYMBOL: symbol,
            OPEN_TIME: entry[0] / 1000,
            OPEN: entry[1],
            HIGH: entry[2],
            LOW: entry[3],
            CLOSE: entry[4],
            VOLUME: entry[5]
          });
        }
      });
      i++;
      // store latest time captured for current symbol in state object
      STATE[symbol].t = apiRes.data[apiRes.data.length - 1][0];
    }
  } catch (err) {
    thisErr = [
      `ERROR: retrieving data from api / creating formattedData object: `,
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
  }

  // save new data to db
  try {
    // initialize sql transaction for saving to db with async / await
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await table_schema.sync({ force: false }, tx);
    await table_schema.bulkCreate(formattedData, tx).then((data, tx) => {
      console.log(
        `4: New klines interval=1m data successfully saved to db ${DB_TABLE_NAME}`
      );
    });
    await tx.commit();
  } catch (err) {
    thisErr = [`ERROR: saving new data object to db ${DB_TABLE_NAME} `, err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // connect to db > state table and store state object for next loop
  try {
    tx = await sequelize.transaction();
    await state_table
      .findOne({ where: { NAME: `${STATE_VAR_NAME}` }, tx })
      .then((entry, tx) => {
        console.log('5: state object entry to be updated: ', entry);
        entry
          .update({ DATA: STATE }, { where: { NAME: `${STATE_VAR_NAME}` }, tx })
          .then((res, tx) =>
            console.log('6: state object entry update completed')
          );
      });
    await tx.commit();
  } catch (err) {
    thisErr = ['ERROR: saving updated state object: ', err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // close off all connections to db
  await sequelize.close();
  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    // throw errArr;
    callback(errArr, null);
  } else {
    // return 'function completed';
    callback(null, 'function completed');
  }
};
