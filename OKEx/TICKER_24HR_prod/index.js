exports.handler = async function(event, context, callback) {
  /* ONLY FOR PROD */ context.callbackWaitsForEmptyEventLoop = false;

  // store ALL env vars for use throughout script
  // permanent
  const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE;
  const POSTGRES_USER = process.env.POSTGRES_USER;
  const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
  const POSTGRES_HOST = process.env.POSTGRES_HOST;
  // function specific
  const DB_TABLE_NAME = process.env.DB_TABLE_NAME;
  const N = process.env.DECIMAL_MAX_DIGITS;
  const EXCHANGE_SYMBOLS = process.env.EXCHANGE_SYMBOLS.split(',');

  // import packages
  const axios = require('axios');
  const pg = require('pg');
  const Sequelize = require('sequelize');

  // initialize sequelize w db config
  const sequelize = new Sequelize(
    POSTGRES_DATABASE,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    {
      host: POSTGRES_HOST,
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        acquire: 10000,
        idle: 1000
      },
      operatorsAliases: false,
      logging: false
    }
  );

  // define table schema for this api
  const table_schema = sequelize.define(DB_TABLE_NAME, {
    UUID: {
      type: Sequelize.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    SYMBOL: Sequelize.STRING,
    TIMESTAMP: Sequelize.INTEGER,
    BUY: Sequelize.DECIMAL(N, 8),
    HIGH: Sequelize.DECIMAL(N, 8),
    LAST: Sequelize.DECIMAL(N, 8),
    LOW: Sequelize.DECIMAL(N, 8),
    SELL: Sequelize.DECIMAL(N, 8),
    VOL_24HR: Sequelize.DECIMAL(N, 8)
  });
  // var for storing formatted api data;
  let formattedData = [],
    i = 1;
  // error handling vars
  let errArr = [],
    thisErr = [];

  // pull api data and format for saving to db
  try {
    for (let symbol of EXCHANGE_SYMBOLS) {
      const url = `https://www.okex.com/api/v1/ticker.do?symbol=${symbol}`;
      console.log(
        `(${i}/${
          EXCHANGE_SYMBOLS.length
        }): GETting symbol from ${url} @ ${Date.now()}`
      );
      const apiRes = await axios.get(url);
      const ticker = apiRes.data.ticker;
      formattedData.push({
        SYMBOL: symbol,
        TIMESTAMP: apiRes.data.date,
        BUY: ticker.buy,
        HIGH: ticker.high,
        LAST: ticker.last,
        LOW: ticker.low,
        SELL: ticker.sell,
        VOL_24HR: ticker.vol
      });
      i++;
    }
  } catch (err) {
    thisErr = [
      `ERROR: retrieving data from api / creating formattedData object: `,
      err
    ];
    errArr.push(thisErr);
    // console.log(thisErr);
  }

  // sequelize async / await enabler var
  let tx;

  // save data to DB
  try {
    tx = await sequelize.transaction();
    // creates DB if none exists yet
    await table_schema.sync({ force: false }, tx);
    await table_schema.bulkCreate(formattedData, tx).then((data, tx) => {
      console.log(
        `New Ticker / 24hr Volume data successfully saved to db ${DB_TABLE_NAME}`
      );
    });
    await tx.commit();
  } catch (err) {
    thisErr = [`ERROR: saving new data to db ${DB_TABLE_NAME}: `, err];
    errArr.push(thisErr);
    // console.log(thisErr);
    await tx.rollback();
  }

  // close off all connections to db
  await sequelize.close();
  // if there were any errors, throw error so I get a notification
  if (errArr.length > 0) {
    // throw errArr;
    callback(errArr, null);
  } else {
    // return 'function completed';
    callback(null, 'function completed');
  }
};
